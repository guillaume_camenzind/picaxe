init: 
  servo 2,70  ; initialise servo
main: 
  servopos c.2,70 ; move servo to one end
  pause 2000  ; wait 2 seconds
  servopos 2,71
  pause 2000  ; wait 2 seconds
  goto main ; loop back to start
